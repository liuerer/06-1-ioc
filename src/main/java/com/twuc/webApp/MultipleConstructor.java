package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private String message;
    private Dependent dependent;

    public MultipleConstructor(String message) {
        this.message = message;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public String getMessage() {
        return message;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
