package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SimpleObject implements SimpleInterface {

    private SimpleDependent simpleDependent;

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }
}
