package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private Dependent dependent;
    private AnotherDependent anotherDependent;

    public Dependent getDependent() {
        return dependent;
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent){
        this.anotherDependent = anotherDependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
    }
}

