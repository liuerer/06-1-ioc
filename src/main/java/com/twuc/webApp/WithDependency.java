package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithDependency {

    @Autowired
    private  Dependent dependent;

    public Dependent getDependent() {
        return dependent;
    }
}
