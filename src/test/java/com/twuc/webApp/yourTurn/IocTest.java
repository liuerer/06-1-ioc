package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class IocTest {

    AnnotationConfigApplicationContext context;

    @BeforeEach
    private void inti_context(){
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_get_without_dependency_object() {

        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
    }

    @Test
    void should_get_with_dependency_object() {

        WithDependency bean = context.getBean(WithDependency.class);

        assertNotNull(bean);
        assertNotNull(bean.getDependent());
    }

    @Test
    void should_get_out_of_scanning_scope() {
        assertThrows(RuntimeException.class, () -> {
            OutOfScanningScope bean = context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_verify_call_interface_bean_for_interface_impl() {
        Interface bean = context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void should_get_simple_dependent_name() {
        SimpleInterface bean = context.getBean(SimpleInterface.class);

        assertNotNull(bean);

        SimpleObject simpleObject = (SimpleObject) bean;

        assertEquals("O_o", simpleObject.getSimpleDependent().getName());
    }

    @Test
    void should_construct_by_dependent() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);

        assertNotNull(bean.getDependent());
        assertNull(bean.getMessage());
    }

    @Test
    void should_verify_whether_initialize_be_called() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);

        assertNotNull(bean);
        assertNotNull(bean.getDependent());
        assertNotNull(bean.getAnotherDependent());
    }

    @Test
    void should_verify_interface_with_multi_implement() {
        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);

        beans.forEach((key, value) -> {
            if(key.equals("ImplementationA"))
                assertSame(ImplementationA.class, value.getClass());
            else if(key.equals("ImplementationB"))
                assertSame(ImplementationB.class, value.getClass());
            else if(key.equals("ImplementationC"))
                assertSame(ImplementationC.class, value.getClass());
            else
                fail();
        });
    }
}

